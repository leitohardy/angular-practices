import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {NgHelloPageComponent} from "./ng-hello-page/ng-hello-page.component";
import {ErrorPageComponent} from "./error-page/error-page.component";
import {FormsComponent} from "./forms/forms.component";
import {RxjsComponent} from "./rxjs/rxjs.component";
import {NgrxComponent} from "./ngrx/ngrx.component";
import {DirectivesComponent} from "./directives/directives-page/directives.component";
import {InjectionsComponent} from "./injection/injections/injections.component";

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'ng-hello', component: NgHelloPageComponent },
  { path: 'injection', component: InjectionsComponent },
  { path: 'forms', component: FormsComponent },
  // { path: 'directives', component: DirectivesComponent },
  { path: 'directives', loadComponent: () => DirectivesComponent },
  { path: 'rxjs', component: RxjsComponent },
  { path: 'ngrx', component: NgrxComponent },
  { path: '**', component: ErrorPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
