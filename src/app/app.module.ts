import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgHelloPageComponent } from './ng-hello-page/ng-hello-page.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { provideHttpClient, withInterceptorsFromDi } from "@angular/common/http";
import {MatModule} from "./modules/mat.module";
import { FormsComponent } from './forms/forms.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { NgrxComponent } from './ngrx/ngrx.component';
import { StoreModule } from '@ngrx/store';
import { todoReducer } from "./store/todo.reducer";

@NgModule({
    declarations: [
        AppComponent,
        NgHelloPageComponent,
        ErrorPageComponent,
        FormsComponent,
        RxjsComponent,
        NgrxComponent,
    ],
    bootstrap: [AppComponent], imports: [BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        MatModule,
        StoreModule.forRoot({todos: todoReducer})],
  exports: [],
    providers: [provideHttpClient(withInterceptorsFromDi())]
})
export class AppModule {}
