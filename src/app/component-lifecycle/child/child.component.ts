import { Component } from '@angular/core';

@Component({
  selector: 'app-child',
  standalone: true,
  imports: [],
  templateUrl: './child.component.html',
  styleUrl: './child.component.scss'
})
export class ChildComponent {

  constructor() {
    console.log('[child] - constructor');
  }

  ngOnInit() {
    //console.log('[child] - [LifecycleComponent] ngOnInit');
    console.log('[child] - ngOnInit');
  }
  ngDoCheck() {
    console.log('[child] - ngDoCheck');
  }

  ngOnChanges() {
    console.log('[child] - ngOnChanges');
  }

  ngAfterViewInit() {
    console.log('[child] - ngAfterViewInit');
  }

  ngAfterViewChecked(){
    console.log('[child] - ngAfterViewChecked');
  }

  /*
   The ngAfterContentInit method runs once after all the children nested inside the component (its content) have been initialized.
   You can use this child hook to read the results of content queries. While you can access the initialized state of these queries,
   attempting to change any state in this method results in an ExpressionChangedAfterItHasBeenCheckedError
 */
  ngAfterContentInit () {
    console.log('[child] - ngAfterContentInit');
  }

  ngAfterContentChecked() {
    console.log('[child] - ngAfterContentChecked');
  }

  afterNextRender(){
    console.log('[child] - afterNextRender');
  }

  afterRender() {
    console.log('[child] - afterRender');
  }

  ngOnDestroy() {
    console.log('[child] - [LifecycleComponent] ngOnDestroy');
  }
}
