import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChildComponent} from "./child/child.component";

@Component({
  selector: 'app-component-lifecycle',
  standalone: true,
  imports: [
    ChildComponent
  ],
  templateUrl: './lifecycle.component.html',
  styleUrl: './lifecycle.component.scss'
})
export class LifecycleComponent {
  hooks: string[] = [];
  isListVisible = false;

  showList() {
    this.isListVisible = !this.isListVisible;
  }

  constructor() {
    console.log('[lifecycle] - constructor');
  }

  ngOnInit() {
    //console.log('[lifecycle] - [LifecycleComponent] ngOnInit');
    console.log('[lifecycle] - ngOnInit');
  }
  ngDoCheck() {
    console.log('[lifecycle] - ngDoCheck');
  }

  ngOnChanges() {
    console.log('[lifecycle] - ngOnChanges');
  }

  ngAfterViewInit() {
    console.log('[lifecycle] - ngAfterViewInit');
  }

  ngAfterViewChecked(){
    console.log('[lifecycle] - ngAfterViewChecked');
  }

  /*
   The ngAfterContentInit method runs once after all the children nested inside the component (its content) have been initialized.
   You can use this lifecycle hook to read the results of content queries. While you can access the initialized state of these queries,
   attempting to change any state in this method results in an ExpressionChangedAfterItHasBeenCheckedError
 */
  ngAfterContentInit () {
    console.log('[lifecycle] - ngAfterContentInit');
  }

  ngAfterContentChecked() {
    console.log('[lifecycle] - ngAfterContentChecked');
  }

  afterNextRender(){
    console.log('[lifecycle] - afterNextRender');
  }

  afterRender() {
    console.log('[lifecycle] - afterRender');
  }

  ngOnDestroy() {
    console.log('[lifecycle] - [LifecycleComponent] ngOnDestroy');
  }

}
