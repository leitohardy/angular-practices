import {Component, Input} from '@angular/core';
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  standalone: true,
  imports: [
    NgIf
  ]
})
export class ButtonComponent {
  isVisible = true;

  @Input() btnName = 'default btn name';
  @Input() disable = true;


}
