import {Directive, ElementRef, Input, Renderer2, signal} from '@angular/core';
import {ButtonComponent} from "../components/button/button.component";

@Directive({
  selector: '[appAdvancedBtn]',
  standalone: true
})
export class AdvancedBtnDirective {
  //customName = signal<string>('name from directive!');

  @Input('appMyDirectiveCustomName') customName = 'default name';


  constructor(private btnComponent: ButtonComponent,
              public el: ElementRef,
              private renderer: Renderer2) {

    this.btnComponent.btnName = this.customName;
    this.btnComponent.isVisible = true;
    this.btnComponent.disable = false;
  }

  ngOnInit() {
    this.btnComponent.btnName = this.customName;
  }

  ngAfterViewInit() {
    const button = this.el.nativeElement.querySelector('button');
    this.renderer.addClass(button, 'advancedBtn');
  }

}
