import { Component } from '@angular/core';
import {HighlightDirective} from "../highlight.directive";
import {UserEventDirective} from "../user-event.directive";
import {WithInputDirective} from "../with-input.directive";
import {AdvancedBtnDirective} from "../advanced-btn.directive";
import {ButtonComponent} from "../../components/button/button.component";

@Component({
  selector: 'app-directives-page',
  standalone: true,
  imports: [HighlightDirective, UserEventDirective, WithInputDirective, AdvancedBtnDirective, ButtonComponent],
  templateUrl: './directives.component.html',
  styleUrl: './directives.component.scss'
})
export class DirectivesComponent {
  // 4
  color4directive: string = 'lightblue';

  toggleColorFor_appWithInput() {
    this.color4directive = this.color4directive === 'lightblue' ? 'lightgoldenrodyellow' : 'lightblue';
  }
}
