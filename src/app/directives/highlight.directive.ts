import {AfterViewInit, Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[highlight]',
  standalone: true
})
export class HighlightDirective implements AfterViewInit{

  constructor(private el: ElementRef) { }

  ngAfterViewInit(): void {
    this.el.nativeElement.style.backgroundColor = 'orange';
  }

}
