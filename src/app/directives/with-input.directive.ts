import {AfterViewInit, Directive, effect, ElementRef, input, Input} from '@angular/core';

@Directive({
  selector: '[appWithInput]',
  standalone: true
})
export class WithInputDirective implements AfterViewInit {
  //@Input() appWithInput: string = 'orange';

  appWithInput = input<string>('orange');

  constructor(private el: ElementRef) {

    effect(() => {
      this.el.nativeElement.style.backgroundColor = this.appWithInput();
    });

  }

  ngAfterViewInit(): void {
    this.el.nativeElement.style.backgroundColor = this.appWithInput();
  }

}
