import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UntypedFormBuilder, FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit, AfterViewInit {
  form = this.fb.group({
    cpu: {value: '7700k', disabled: false},
    storage: {value: ['hhd-t1', 'ssd-m2-samsung'], disabled: false},
  });

  processors = [{name: 'i7 7700k', value: '7700k'}];
  storages = [
    {name: 'm2 Samsung', value: 'ssd-m2-samsung'},
    {name: 'wd blue T1', value: 'hhd-t1'},
  ];

  constructor(private fb: UntypedFormBuilder) {}

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }

  submit(data: any) {
    console.log(data);
  }

}
