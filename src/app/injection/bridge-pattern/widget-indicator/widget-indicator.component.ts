import {Component, forwardRef} from '@angular/core';
import {Widget} from "../widget-wrapper/widget.interface";
import {WIDGET} from "../widget-wrapper/widget.token";

@Component({
  selector: 'app-widget-indicator',
  standalone: true,
  imports: [],
  templateUrl: './widget-indicator.component.html',
  styleUrl: './widget-indicator.component.scss',
  providers: [
    {
      provide: WIDGET,
      useExisting: WidgetIndicatorComponent,
    }
  ]
})
export class WidgetIndicatorComponent implements Widget{
  reload() {
    console.log('[WidgetIndicatorComponent] Reload');
  }

  like(): void {
    console.log('[WidgetIndicatorComponent] like');
  }
}
