import {Component} from '@angular/core';
import {Widget} from "../widget-wrapper/widget.interface";
import {WIDGET} from "../widget-wrapper/widget.token";

@Component({
  selector: 'app-widget-weather',
  standalone: true,
  imports: [],
  templateUrl: './widget-weather.component.html',
  styleUrl: './widget-weather.component.scss',
  providers: [
    {
      provide: WIDGET,
      useExisting: WidgetWeatherComponent,
    }
  ]
})
export class WidgetWeatherComponent implements Widget{
  reload() {
    console.log('[WidgetWeatherComponent] Reload');
  }

  like(): void {
    console.log('[WidgetWeatherComponent] like');
  }
}
