import {Component, ContentChild, ViewEncapsulation} from '@angular/core';
import {WidgetWeatherComponent} from "../widget-weather/widget-weather.component";
import {WidgetIndicatorComponent} from "../widget-indicator/widget-indicator.component";
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {WIDGET} from "./widget.token";
import {Widget} from "./widget.interface";

@Component({
  selector: 'app-widget-wrapper',
  standalone: true,
  imports: [
    WidgetWeatherComponent,
    WidgetIndicatorComponent,
    MatCardModule,
    MatButtonModule
  ],
  templateUrl: './widget-wrapper.component.html',
  styleUrl: './widget-wrapper.component.scss',
  encapsulation: ViewEncapsulation.None
})
export class WidgetWrapperComponent {
  @ContentChild(WIDGET) widget!: Widget;

  reload() {
    this.widget.reload();
  }

  like() {
    this.widget.like();
  }
}
