import { Component } from '@angular/core';
import {WidgetIndicatorComponent} from "../bridge-pattern/widget-indicator/widget-indicator.component";
import {WidgetWeatherComponent} from "../bridge-pattern/widget-weather/widget-weather.component";
import {WidgetWrapperComponent} from "../bridge-pattern/widget-wrapper/widget-wrapper.component";

@Component({
  selector: 'app-injections',
  standalone: true,
  imports: [
    WidgetIndicatorComponent,
    WidgetWeatherComponent,
    WidgetWrapperComponent
  ],
  templateUrl: './injections.component.html',
  styleUrl: './injections.component.scss'
})
export class InjectionsComponent {

}
