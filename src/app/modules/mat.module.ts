import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import {MatSliderModule} from "@angular/material/slider";
import {MatNativeDateModule} from "@angular/material/core";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatButtonModule} from '@angular/material/button';

import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatListModule} from '@angular/material/list';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatIconModule,
    MatSliderModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatButtonModule,
    MatCardModule,
    MatSelectModule,
    MatListModule,
    MatInputModule
  ],
  exports: [
    MatIconModule,
    MatSliderModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatButtonModule,
    MatCardModule,
    MatSelectModule,
    MatListModule,
    MatInputModule
  ],
})
export class MatModule { }
