import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgHelloPageComponent } from './ng-hello-page.component';

describe('NgHelloPageComponent', () => {
  let component: NgHelloPageComponent;
  let fixture: ComponentFixture<NgHelloPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgHelloPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgHelloPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
