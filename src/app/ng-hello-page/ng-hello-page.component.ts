import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-hello-page',
  templateUrl: './ng-hello-page.component.html',
  styleUrls: ['./ng-hello-page.component.scss']
})
export class NgHelloPageComponent implements OnInit {
  title = 'app';
  constructor() { }

  ngOnInit(): void {
  }

}
