import { Component, OnInit } from '@angular/core';
import { MatSelectionListChange} from "@angular/material/list";
import { TodoListService } from "./todo-list.service";
import { Store } from '@ngrx/store';
import { selectTodoCollection } from "../store/todo.selectors";

@Component({
  selector: 'app-ngrx',
  templateUrl: './ngrx.component.html',
  styleUrls: ['./ngrx.component.scss'],
  providers: [TodoListService]
})
export class NgrxComponent implements OnInit {
  todoList = this.todoListService.getList();
  newTodo = '';
  selected: string[] = [];
  // need to continue and remove old methods for todo
  todosCollection$ = this.store.select(selectTodoCollection);

  constructor(
    private readonly todoListService: TodoListService,
    private store: Store
  ) {}
  ngOnInit() {
    this.todosCollection$.subscribe(data => {
      console.log('data', data);
    })
  }

  onChange(change: MatSelectionListChange) {
    console.log(change?.source?._value);

    if (Array.isArray(change?.source?._value)) {
      this.selected = [...change?.source?._value];
    }
  }

  addNewTodo() {
    if (this.newTodo.trim() !== '') {
      this.todoList.push(this.newTodo);
      this.newTodo = '';
    }
  }

  removeSelected() {
    for (const selectedTodo of this.selected) {
      let removeIndex = this.todoList.indexOf(selectedTodo);

      if (removeIndex !== -1) {
        this.todoList.splice(removeIndex, 1);
      }
    }

    this.selected = [];
  }
}
