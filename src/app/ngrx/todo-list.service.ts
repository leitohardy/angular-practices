import { Injectable } from '@angular/core';

@Injectable()
export class TodoListService {

  constructor() { }

  getList() {
    return ['Boots2', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers']
  }
}
