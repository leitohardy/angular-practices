import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {of, Subject} from "rxjs";
import {delay, switchMap, mergeMap, exhaustMap, concatMap} from "rxjs/operators";

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RxjsComponent implements OnInit {

  order$$ = new Subject();
  order2$$ = new Subject();
  order3$$ = new Subject();
  order4$$ = new Subject();

  orderCounter = 1;

  // constructor(private cd: ChangeDetectorRef) { }

  ngOnInit(): void {

    this.order$$.pipe(
      switchMap(order => {
        return of('order: ' + order + ' is complete').pipe(delay(3000));

      })
    ).subscribe(data => console.log(data));


    this.order2$$.pipe(
      mergeMap(order => {
        return of('[mergeMap] order: ' + order + ' is complete').pipe(delay(3000));
      })
    ).subscribe(data => console.log(data));

    this.order3$$.pipe(
      exhaustMap(order => {
        return of('[exhaustMap] order: ' + order + ' is complete').pipe(delay(3000));
      })
    ).subscribe(data => console.log(data), );


    // Que

    this.order4$$.pipe(
      concatMap(order => {
        return of('[concatMap] order: ' + order + ' is complete').pipe(delay(3000));
      })
    ).subscribe(data => console.log(data));


  }

  order() {
    this.order$$.next(this.orderCounter++);
  }

  order2() {
    this.order2$$.next(this.orderCounter++);
  }

  order3() {
    this.order3$$.next(this.orderCounter++);
  }

  order4() {
    this.order4$$.next(this.orderCounter++);
  }

  // ngDoCheck() {
  //   console.log('ngDoCheck !!');
  //   this.cd.markForCheck();
  // }

}
