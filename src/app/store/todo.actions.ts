import { createActionGroup, props } from '@ngrx/store';
import {TODO} from "./todo.model";

export const todoActions = createActionGroup({
  source: 'Todo',
  events: {
    'Add todo': props<TODO>(),
    'Remove todo': props<TODO>(),
  },
});
