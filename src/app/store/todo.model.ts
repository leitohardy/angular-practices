export interface TODO {
  id: number,
  name: string,
  isDone: boolean
}
