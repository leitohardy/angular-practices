import { createReducer, on } from '@ngrx/store';
import { todoActions } from './todo.actions';
import {TODO} from "./todo.model";

export const initialState: ReadonlyArray<TODO> = [
  {
    id: 12321,
    name: 'test',
    isDone: false
  }
];

export const todoReducer = createReducer(
  initialState,
  on(todoActions.removeTodo, (state, payload) => {
      return state.filter((todo) => todo.id !== payload.id)
  }
  ),
  on(todoActions.addTodo, (state, payload) => {
    return [...state, payload];
  })
);
