import { createSelector, createFeatureSelector } from '@ngrx/store';
import {TODO} from "./todo.model";

export const selectTodos = createFeatureSelector<ReadonlyArray<TODO>>('todos');


export const selectTodoCollection = createSelector(
  selectTodos,
  (todos) => todos
  // selectCollectionState,
  // (books, collection) => {
  //   return collection.map((id) => books.find((book) => book.id === id));
  // }
);
